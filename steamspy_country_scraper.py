
#####################

from lxml import html
import re
import csv
import subprocess
import time
import sys

countries = dict()
if len(sys.argv) == 1: # if not in debug mode
    with open("country_codes.csv") as csvfile:
        reader = csv.DictReader(csvfile,fieldnames=['Name','Code'])
        next(reader,None) # skip the headers
        for row in reader:
            name = row['Name']
            name = re.sub(", [A-Za-z ]*","",name) # for countries inconveiniently named "North Korea, The Democratic Peoples Republic of"
            countries[row['Name']] = row['Code']
else: 
    countries["United States"] = "us"
    countries["Bolivia"] = "bo"


first = True
for country in sorted(countries):

    # I was going to use urllib  or requests library to read it but it wouldnt work
    # but there is always a less elegant way
    url = "http://steamspy.com/country/" + countries[country] 
    subprocess.call(["curl",url],stdout=open("tempUrl.txt","w",0)) # write to a temp file
    F = open("tempUrl.txt")
    tree = html.fromstring(F.read())
    F.close()
    subprocess.call(["rm","tempUrl.txt"]) # remove temp file
    
    if first:
        fields = tree.xpath('//div[@class="panel-body"]/*/text()')
        fields = fields[0:9]
        fields[4] = fields[4] +  " " + fields[5] # adding and removing the "(two weeks)"
        fields[7] = fields[7] +  " " + fields[8] # adding and removing the "(two weeks)"
        del fields[-1]
        del fields[5]
        fields = ["country"] + fields
        print ",".join(fields)
        first = False
        
    info = tree.xpath('//div[@class="panel-body"]/text()')
    info = [x for x in info if not re.match("\n| \($",x)]
    info = info[0:7]
    data = list()
    for entry in info:
        entry = re.sub(".*?: ","", entry)
        entry = re.sub("%","", entry)
        if(re.match("[0-9]+:[0-9]+",entry)):
            (hours,mins) =  re.split(":",entry)
            # convert times to hours
            entry = float(hours) + float(mins)*(1.0/60.0) 
        data.append(str(entry))

    data = [country] + data 
    print ",".join(data)
    time.sleep(3)
